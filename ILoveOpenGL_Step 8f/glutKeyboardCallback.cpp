#include "globals.h"

#include <iostream>

// GLUT keyboard callback handler
//*********************************************************************
//Simple callback to handl key presses that should be single events
//*********************************************************************

void myKeyboardCallback(unsigned char keyCode, int x, int y)
{

	if (g_bCtrlPressed && keyCode == '+')
	{
		::g_selectedLightIndex++;
		if (::g_selectedLightIndex >= NUMBEROFLIGHTS)
		{
			::g_selectedLightIndex = NUMBEROFLIGHTS;
		}
	}
	else if (g_bCtrlPressed && keyCode == '-')
	{
		::g_selectedLightIndex--;
		if (::g_selectedLightIndex <= 0)
		{
			::g_selectedLightIndex = 0;
		}
	}
	else if (!g_bCtrlPressed && keyCode == '+')
	{
		::g_selectedObject++;
		if (::g_selectedObject >= g_vec_pGOs.size() - 1)
		{
			::g_selectedObject = g_vec_pGOs.size() - 1;
		}
	}
	else if (!g_bCtrlPressed && keyCode == '-')
	{
		g_selectedObject--;
		if (g_selectedObject <= 0)
		{
			g_selectedObject = 0;
		}
	}

	switch (keyCode)
	{
	case '[':	// Turn on light debugging
		::g_bDebugLights = true;
		break;
	case ']':
		::g_bDebugLights = false;
		break;
		//Change selected Object
	default:
		//Because it's just the right thing to do...
		break;
	}

	return;
}

//***********************************************************************
//Special Keyboard callback shows some different ways to access
//***********************************************************************

void mySpecialKeyboardCallback(int key, int x, int y)
{
	float camSpeed = 0.01f;

	switch (key)
	{
	case GLUT_KEY_CTRL_L:
	case GLUT_KEY_CTRL_R:
		if (g_bCtrlPressed == true)
		{
			g_bCtrlPressed = false;
			std::cout << "CTRL OFF" << std::endl;
		}
		else
		{
			g_bCtrlPressed = true;
			std::cout << "CTRL ON" << std::endl;
		}
		break;
	case GLUT_KEY_UP:		// "Away??"
		::g_cam_eye.z += camSpeed;
		break;
	case GLUT_KEY_DOWN:
		::g_cam_eye.z -= camSpeed;
		break;

	case GLUT_KEY_LEFT:		// "Away??"
		::g_cam_eye.x -= camSpeed;
		break;
	case GLUT_KEY_RIGHT:
		::g_cam_eye.x += camSpeed;
		break;

	case GLUT_KEY_PAGE_UP:
		::g_cam_eye.y += camSpeed;
		break;
	case GLUT_KEY_PAGE_DOWN:
		::g_cam_eye.y -= camSpeed;
		break;
	}

	return;
}