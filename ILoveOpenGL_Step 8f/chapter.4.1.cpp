#include "globals.h"
#define WINDOW_TITLE_PREFIX "Final pre exam dev"

#include <iostream>
#include <sstream>
#include <fstream>

// To load the ply file
#include <string>
#include <fstream>
#include <vector>
#include "cVertex.h"
#include "cTriangle.h"
#include <sstream>		// Why isn't it stringstream.... really?
#include <iomanip>

//void LoadPlyFile(std::string filename, 
//	             std::vector< cVertex > &vecVerts,
//	             std::vector< cTriangle > &vecTris, 
//	             int &numberOfVerts, 
//	             int &numberOfTriangle );

//std::vector< cVertex >   g_vecVerts;
//std::vector< cTriangle > g_vecTris;
//int g_numberOfVerts = 0;
//int g_numberOfTriangles = 0;

#include "cGameObject.h"
#include "cMeshManager.h"
#include "CShaderManager/CGLShaderManager.h"	// Note: it's "C" here

#include <sstream>

cMeshManager* g_pTheMeshManager = 0;

IGLShaderManager* g_pTheShaderManager = 0;
CTextureManager* g_pTheTextureManager = 0;

cGameObject* g_pDebugBall = 0;


int
  CurrentWidth = 800,
  CurrentHeight = 600,
  WindowHandle = 0;

unsigned FrameCount = 0;

GLint  ProjectionMatrixUniformLocation = 0;
GLint  ViewMatrixUniformLocation = 0;
GLint  ModelMatrixUniformLocation = 0;
GLint  ModelMatrixRotationOnlyUniformLocation = 0;

std::vector<cLightDesc> g_vecLights;




// Fragment shader uniforms...
// For directly setting the ambient and diffuse (if NOT using textures)
GLint UniLoc_MaterialAmbient_RGB = 0;
GLint UniLoc_MaterialDiffuse_RGB = 0;
GLint UniLoc_MaterialSpecular = 0;
GLint UniLoc_MaterialShininess = 0;
GLint UniLoc_eye = 0;
GLint UniLoc_debugColour = 0;
GLint UniLoc_bUseDebugColour = 0;
GLint UniLoc_bUseVertexRGBAColours = 0;
GLint UniLoc_bUseTextureMaterials = 0;	
GLint UniLoc_bUseTexturesOnly = 0;

GLint UniLoc_myAlphaAllObject = 0;
GLint UniLoc_bAlphaForEntireObject = 0;

GLint UniLoc_bUseDiscardMask = 0;

// This is taken from the shader... (the fact we have 8 samplers)
// NOTE: we are using an array here, but you CAN'T have sampler arrays
//	in this way inside the shader. There are things called "texture arrays",
//	but they are NOT the same thing at all. 
static const unsigned int NUMBEROF2DSAMPLERS = 8;
GLint UniLoc_texSampler2D[NUMBEROF2DSAMPLERS] = {0};
GLint UniLoc_texMix[NUMBEROF2DSAMPLERS] = {0};

glm::mat4 matProjection;
glm::mat4 matView;			// "camera"
glm::mat4 matWorld;			// "model"

float CubeRotation = 0;
clock_t LastTime = 0;

void Initialize(int, char*[]);
void InitWindow(int, char*[]);
void ResizeFunction(int, int);
void RenderFunction(void);
void TimerFunction(int);
void IdleFunction(void);

void SetupShader(void);

//void CreateCube(void);
//bool LoadMeshIntoVBO(std::string fileToLoad, unsigned int &VBO);

//void DestroyCube(void);
void ShutErDownPeople(void);

// Add keyboard and mouse callbacks
void myKeyboardCallback( unsigned char keyCode,
	                     int x, int y );
// for arrow, PigUp, PigDown, F1, etc.
void mySpecialKeyboardCallback(int key, int x, int y);

void myMouseCallback();		// Look into this if you like

//void DrawCube(void);
void DrawObject(cGameObject* pGO);

void CreateDefaultObjects(void);
void SetUpInitialLightValues(void);

//Add function declarations later!!

glm::vec3 defaultValue3f = { 0.0f, 0.0f, 0.0f };
glm::vec3 defaultValueSpec3f = { 1.0f, 1.0f, 1.0f };

glm::vec4 defaultValue04f = { 1.0f, 1.0f, 1.0f, 1.0f };	//default pos

glm::vec4 defaultValue4f = { 1.0f, 1.0f, 1.0f, 1.0f };	//default colour white
glm::vec4 defaultValueAmb4f = { 0.2f, 0.2f, 0.2f, 1.0f };	//default colour white

glm::vec4  defaultValue14f = { 0.0f, 0.0f, 0.0f, 1.0f };//diffuse and debug colour black

void createLight(int index, int lightType,
	glm::vec4 pos = defaultValue04f, 
	glm::vec4 diff = defaultValue4f, 
	glm::vec4 amb = defaultValueAmb4f, 
	glm::vec4 spec = defaultValue4f, 
	float attenConst = 0.0f, 
	float attenLinear = 0.0f, 
	float attenQuad = 1.0f)
{
	//Should a light be created with a placeholder object bound to its location?, would also need to be updated in the render fucntion

	if (index < NUMBEROFLIGHTS)
	{
		if (lightType == 1)
		{
			::g_vecLights[index].direction = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
			::g_vecLights[index].anglePenumbraEnd = 20.0f;
				::g_vecLights[index].anglePenumbraStart = 15.0f;
				::g_vecLights[index].lightType = lightType;
		}
		else if (lightType == 0)
			::g_vecLights[index].lightType = lightType;
		else if (lightType == 2)
			::g_vecLights[index].lightType = lightType;

		::g_vecLights[index].position = pos;
		::g_vecLights[index].diffuse = diff;
		::g_vecLights[index].ambient = amb;
		::g_vecLights[index].specular = spec;
		::g_vecLights[index].attenConst = attenConst;
		::g_vecLights[index].attenLinear = attenLinear;
		::g_vecLights[index].attenQuad = attenQuad;
	}
	// Assume the lights are loaded into the g_vecLights;

}

std::vector<float> vecTextureMixRatiosDefault(NUMBEROF2DSAMPLERS, 0.0f);

//Name, Colours, Scale, Position, Rotations
void createObject(std::string plyName,
	glm::vec4 dbgColour = defaultValue14f,
	glm::vec4 diffColour = defaultValue4f,
	glm::vec4 ambColour = defaultValueAmb4f,
	glm::vec3 specColour = defaultValueSpec3f,
	float shininess = 1.0f,
	float scale = 1.0f,
	glm::vec3 position = defaultValue3f,
	glm::vec3 preRot = defaultValue3f,
	glm::vec3 postRot = defaultValue3f,
	bool useDiscardMask = 0,
	std::vector<float> vecTextureMixRatios[NUMBEROF2DSAMPLERS] = &vecTextureMixRatiosDefault,
	bool useTexturesAsMaterials = 0,
	bool useTexturesWithNoLighting = 0,
	bool isEntirelyTransparent = 0,
	float alphaValue = 1.0f
	)
{
	cGameObject* obj = new cGameObject();

	//ply model to be loaded for the object
	obj->modelName = plyName;
	//
	obj->debugColour = dbgColour;

	//Set object properties(how it reacts to light)
	obj->diffuse = diffColour;
	obj->ambient = ambColour;
	obj->specular = specColour;
	obj->shininess = shininess; //1 - 100,000
								//Object size
	obj->scale = scale;
	//Object position, ASSUMES position is passed in as x,y,z order
	obj->position = position;
	//Object rotation values, also assumes x,y,z order
	obj->preRotation = preRot;
	obj->postRotation = postRot;

	obj->bUseDiscardMask = useDiscardMask;		
	obj->ClearTextureMixValues(NUMBEROF2DSAMPLERS, 0.0f);
	obj->vecTextureMixRatios = *vecTextureMixRatios;	
	obj->bUseTexturesAsMaterials = useTexturesAsMaterials;	
	obj->bUseTexturesWithNoLighting = useTexturesWithNoLighting;
	obj->bIsEntirelyTransparent = isEntirelyTransparent;
	obj->alphaValue = alphaValue;

	//Add model to vector to be drawn
	::g_vec_pGOs.push_back(obj);
}
//
//

void readFile(std::string path)
{

	std::ifstream fileIn;
	fileIn.open(path);

	if (fileIn.is_open()) {
		while (fileIn) {
			//read a line of the file
			std::string s;
			std::string holdStr;
			std::string val1;
			std::string val2;
			std::string val3;
			std::string val4;

			//if there is a new line
			if (!getline(fileIn, s)) break;

			//Change this lame setup, better structure and organization
			std::istringstream ss(s);

			while (ss) {
				if (!getline(ss, holdStr, ',')) break;//if no more values

													  //getline(fileIn, holdStr, ',');
				if (holdStr == "obj")
				{
					getline(ss, holdStr, ',');
					std::string plyName(holdStr);

					getline(ss, val1, ',');
					getline(ss, val2, ',');
					getline(ss, val3, ',');
					getline(ss, val4, ',');
					glm::vec4 dbgColour = { std::stof(val1), std::stof(val2), std::stof(val3), std::stof(val4) };

					getline(ss, val1, ',');
					getline(ss, val2, ',');
					getline(ss, val3, ',');
					getline(ss, val4, ',');
					glm::vec4 diffColour = { std::stof(val1), std::stof(val2), std::stof(val3), std::stof(val4) };

					getline(ss, val1, ',');
					getline(ss, val2, ',');
					getline(ss, val3, ',');
					getline(ss, val4, ',');
					glm::vec4 ambColour = { std::stof(val1), std::stof(val2), std::stof(val3), std::stof(val4) };

					getline(ss, val1, ',');
					getline(ss, val2, ',');
					getline(ss, val3, ',');
					glm::vec3 specColour = { std::stof(val1), std::stof(val2), std::stof(val3) };

					getline(ss, val1, ',');
					float shininess = std::stof(val1);

					getline(ss, val1, ',');
					float scale = std::stof(val1);

					getline(ss, val1, ',');
					getline(ss, val2, ',');
					getline(ss, val3, ',');
					glm::vec3 position = { std::stof(val1), std::stof(val2), std::stof(val3) };

					getline(ss, val1, ',');
					getline(ss, val2, ',');
					getline(ss, val3, ',');
					glm::vec3 preRot = { std::stof(val1), std::stof(val2), std::stof(val3) };

					getline(ss, val1, ',');
					getline(ss, val2, ',');
					getline(ss, val3, ',');
					glm::vec3 postRot = { std::stof(val1), std::stof(val2), std::stof(val3) };

					getline(ss, val1, ',');
					bool bUseDiscardMask = std::stoi(val1);

					getline(ss, val1, ',');
					bool bUseTexturesAsMaterials = std::stoi(val1);

					getline(ss, val1, ',');
					bool bUseTexturesWithNoLighting = std::stoi(val1);

					getline(ss, val1, ',');
					bool bIsEntirelyTransparent = std::stoi(val1);

					getline(ss, val1, ',');
					float alphaValue = std::stof(val1);

					std::vector<float> vecTextureMixRatios;
					while (getline(ss, val1, ','))
					{
						vecTextureMixRatios.push_back(std::stof(val1));
					}
					if (vecTextureMixRatios.size() > NUMBEROF2DSAMPLERS)
						bUseTexturesAsMaterials = 0;//the file was saved with different numbers of textures loaded and could crash if used

					createObject(plyName, dbgColour, diffColour, ambColour, specColour, shininess, scale, position, preRot, postRot, bUseDiscardMask, &vecTextureMixRatios, bUseTexturesAsMaterials, bUseTexturesWithNoLighting, bIsEntirelyTransparent, alphaValue);
				}

				else if (holdStr == "light")
				{
					//load light from file and create a light
					getline(ss, val1, ',');
					int index = std::stoi(val1);

					getline(ss, val1, ',');
					int lightType = std::stoi(val1);

					getline(ss, val1, ',');
					getline(ss, val2, ',');
					getline(ss, val3, ',');
					getline(ss, val4, ',');
					glm::vec4 position = { std::stof(val1), std::stof(val2), std::stof(val3), std::stof(val4) };

					getline(ss, val1, ',');
					getline(ss, val2, ',');
					getline(ss, val3, ',');
					getline(ss, val4, ',');
					glm::vec4 ambient = { std::stof(val1), std::stof(val2), std::stof(val3), std::stof(val4) };


					getline(ss, val1, ',');
					getline(ss, val2, ',');
					getline(ss, val3, ',');
					getline(ss, val4, ',');
					glm::vec4 diffuse = { std::stof(val1), std::stof(val2), std::stof(val3), std::stof(val4) };

					getline(ss, val1, ',');
					getline(ss, val2, ',');
					getline(ss, val3, ',');
					getline(ss, val4, ',');
					glm::vec4 specular = { std::stof(val1), std::stof(val2), std::stof(val3), std::stof(val4) };

					getline(ss, val1, ',');
					float attenConst = std::stof(val1);

					getline(ss, val1, ',');
					float attenLinear = std::stof(val1);

					getline(ss, val1, ',');
					float attenQuad = std::stof(val1);

					createLight(index, lightType, position, diffuse, ambient, specular, attenConst, attenLinear, attenQuad);
				}
			}//end csv read while
		}
		std::cout << "File loaded /exit to resume render." << std::endl;
	}
	else
	{
		std::cout << "ReadFile failed, file not found or could not open." << std::endl;
	}
	fileIn.close();
	return;
}

void writeFile(std::string path)
{
	std::ofstream fileOut;
	//Mode decides how the additions are saved to the file
	/*
	ios::in			Open for input operations.
	ios::out		Open for output operations.
	ios::binary		Open in binary mode.
	ios::ate		Set the initial position at the end of the file.
	If this flag is not set, the initial position is the beginning of the file.
	ios::app		All output operations are performed at the end of the file, appending the content to the current content of the file.
	ios::trunc		If the file is opened for output operations and it already existed, its previous content is deleted and replaced by the new one.
	*/
	fileOut.open(path, std::ios::trunc);
	if (fileOut.is_open()) {
		//vector to load the object and light files
		std::vector<std::string> svecLines;
		std::cout << "Creating object and light text, this may take a few minutes..." << std::endl;
		//Load all the lines in the text file into objects and lights
		std::cout << "Creating object text" << std::endl;
		std::string holdStr;
		for (unsigned i = 0; i <= g_vec_pGOs.size() - 1; ++i)
		{
			holdStr = "obj,";
			holdStr += g_vec_pGOs[i]->modelName + ",";
			holdStr += std::to_string(g_vec_pGOs[i]->debugColour.r) + "," +
				std::to_string(g_vec_pGOs[i]->debugColour.g) + "," +
				std::to_string(g_vec_pGOs[i]->debugColour.b) + "," +
				std::to_string(g_vec_pGOs[i]->debugColour.a) + ",";
			holdStr += std::to_string(g_vec_pGOs[i]->diffuse.r) + "," +
				std::to_string(g_vec_pGOs[i]->diffuse.g) + "," +
				std::to_string(g_vec_pGOs[i]->diffuse.b) + "," +
				std::to_string(g_vec_pGOs[i]->diffuse.a) + ",";
			holdStr += std::to_string(g_vec_pGOs[i]->ambient.r) + "," +
				std::to_string(g_vec_pGOs[i]->ambient.g) + "," +
				std::to_string(g_vec_pGOs[i]->ambient.b) + "," +
				std::to_string(g_vec_pGOs[i]->ambient.a) + ",";
			holdStr += std::to_string(g_vec_pGOs[i]->specular.r) + "," +
				std::to_string(g_vec_pGOs[i]->specular.g) + "," +
				std::to_string(g_vec_pGOs[i]->specular.b) + ",";
			holdStr += std::to_string(g_vec_pGOs[i]->shininess) + ",";
			holdStr += std::to_string(g_vec_pGOs[i]->scale) + ",";
			holdStr += std::to_string(g_vec_pGOs[i]->position.x) + "," +
				std::to_string(g_vec_pGOs[i]->position.y) + "," +
				std::to_string(g_vec_pGOs[i]->position.z) + ",";
			holdStr += std::to_string(g_vec_pGOs[i]->preRotation.x) + "," +
				std::to_string(g_vec_pGOs[i]->preRotation.y) + "," +
				std::to_string(g_vec_pGOs[i]->preRotation.z) + ",";
			holdStr += std::to_string(g_vec_pGOs[i]->postRotation.x) + "," +
				std::to_string(g_vec_pGOs[i]->postRotation.y) + "," +
				std::to_string(g_vec_pGOs[i]->postRotation.z) + ",";
			//Black white discard mask
			if (g_vec_pGOs[i]->bUseDiscardMask)
				holdStr += "1,";
			else
				holdStr += "0,";
			//Use Texture
			if (g_vec_pGOs[i]->bUseTexturesAsMaterials)
				holdStr += "1,";
			else
				holdStr += "0,";
			//Texture no Lights
			if (g_vec_pGOs[i]->bUseTexturesWithNoLighting)
				holdStr += "1,";
			else
				holdStr += "0,";
			//Transparency
			if (g_vec_pGOs[i]->bIsEntirelyTransparent)
				holdStr += "1,";
			else
				holdStr += "0,";
			holdStr += std::to_string(g_vec_pGOs[i]->alphaValue) + ",";

			for (unsigned itex = 0; itex < g_vec_pGOs[i]->vecTextureMixRatios.size(); ++itex)
			{
				holdStr += std::to_string(g_vec_pGOs[i]->vecTextureMixRatios[itex]);
				if(itex != NUMBEROF2DSAMPLERS)
					holdStr +=",";
			}

			//load object in txt form onto vector
			svecLines.push_back(holdStr);

		}//create object txt for loop

		std::cout << "Creating light text" << std::endl;
		for (unsigned i = 0; i <= g_vecLights.size() - 1; ++i)
		{
			holdStr = "light,";
			holdStr += std::to_string(i) + ",";
			holdStr += std::to_string(g_vecLights[i].lightType) + ",";
			holdStr += std::to_string(g_vecLights[i].position.x) + "," +
				std::to_string(g_vecLights[i].position.y) + "," +
				std::to_string(g_vecLights[i].position.z) + "," +
				std::to_string(g_vecLights[i].position.w) + ",";
			holdStr += std::to_string(g_vecLights[i].diffuse.r) + "," +
				std::to_string(g_vecLights[i].diffuse.g) + "," +
				std::to_string(g_vecLights[i].diffuse.b) + "," +
				std::to_string(g_vecLights[i].diffuse.a) + ",";
			holdStr += std::to_string(g_vecLights[i].ambient.r) + "," +
				std::to_string(g_vecLights[i].ambient.g) + "," +
				std::to_string(g_vecLights[i].ambient.b) + "," +
				std::to_string(g_vecLights[i].ambient.a) + ",";
			holdStr += std::to_string(g_vecLights[i].specular.r) + "," +
				std::to_string(g_vecLights[i].specular.g) + "," +
				std::to_string(g_vecLights[i].specular.b) + "," +
				std::to_string(g_vecLights[i].specular.a) + ",";
			holdStr += std::to_string(g_vecLights[i].attenConst) + ",";
			holdStr += std::to_string(g_vecLights[i].attenLinear) + ",";
			holdStr += std::to_string(g_vecLights[i].attenQuad);

			svecLines.push_back(holdStr);
		}//create light txt for loop
		 //truncate the original file and write the first line

		for (unsigned i = 0; i <= svecLines.size() - 1; ++i)
		{
			fileOut << svecLines[i] << std::endl;
		}
		fileOut.close();
		std::cout << "File successfully saved to location." << std::endl;

	}
	fileOut.close();
	return;
}

void loadPly()
{
	// LOAD ply before creating the objects
	::g_pTheMeshManager->LoadPlyIntoVBO("assets/models/Asteroid_001.ply");
	::g_pTheMeshManager->LoadPlyIntoVBO("assets/models/Asteroid_003.ply");
	::g_pTheMeshManager->LoadPlyIntoVBO("assets/models/Asteroid_010.ply");
	::g_pTheMeshManager->LoadPlyIntoVBO("assets/models/asteroids.ply");
	::g_pTheMeshManager->LoadPlyIntoVBO("assets/models/Falcon_Engine_Only.ply");
	::g_pTheMeshManager->LoadPlyIntoVBO("assets/models/Falcon_Windscreen_inner_and_outer.ply");
	::g_pTheMeshManager->LoadPlyIntoVBO("assets/models/Isoshphere.ply");
	::g_pTheMeshManager->LoadPlyIntoVBO("assets/models/Port_Panel.ply");
	::g_pTheMeshManager->LoadPlyIntoVBO("assets/models/Starboard_Panel.ply");
	::g_pTheMeshManager->LoadPlyIntoVBO("assets/models/TIE-fighter.ply");
	::g_pTheMeshManager->LoadPlyIntoVBO("assets/models/Falcon_Bodys.ply");
	::g_pTheMeshManager->LoadPlyIntoVBO("assets/models/TearDropBullet.ply");
	::g_pTheMeshManager->LoadPlyIntoVBO("assets/models/Isoshphere_normalsFacingIn.ply");
	::g_pTheMeshManager->LoadPlyIntoVBO("assets/models/Square.ply");
	::g_pTheMeshManager->LoadPlyIntoVBO("assets/models/Falcon_Windscreen_inner_and_outer.ply");
	

}

int main(int argc, char* argv[])
{
	std::cout << "Preparing OpenGL..." << std::endl;
  Initialize(argc, argv);

  loadPly();
  CreateDefaultObjects();
  readFile("./Space.txt");
  g_vec_pGOs[10]->bUseVertexRGBAColoursAsMaterials = true;
  std::cout << "Loading objects..." << std::endl;
  g_selectedObject = 6;
  //CreateDefaultObjects();


  ::g_cam_at = glm::vec3( 6.781f, 0.0f, 4.744f );
  ::g_cam_eye = glm::vec3(0.8059f, 0.4f, 7.491f);

  g_AniTimer.Reset( );
  g_AniTimer.Start( );

  std::cout << "Boom!" << std::endl;
  glutMainLoop();

  exit(EXIT_SUCCESS);
}

void Initialize(int argc, char* argv[])
{
  GLenum GlewInitResult;

  InitWindow(argc, argv);
  
  glewExperimental = GL_TRUE;
  GlewInitResult = glewInit();

  if (GLEW_OK != GlewInitResult) {
    fprintf(
      stderr,
      "ERROR: %s\n",
      glewGetErrorString(GlewInitResult)
    );
    exit(EXIT_FAILURE);
  }
  
  fprintf(
    stdout,
    "INFO: OpenGL Version: %s\n",
    glGetString(GL_VERSION)
  );

  glGetError();
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  ExitOnGLError("ERROR: Could not set OpenGL depth testing options");

  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);
  glFrontFace(GL_CCW);
  ExitOnGLError("ERROR: Could not set OpenGL culling options");

//  ModelMatrix = IDENTITY_MATRIX;
//  ProjectionMatrix = IDENTITY_MATRIX;
//  ViewMatrix = IDENTITY_MATRIX;
//  TranslateMatrix(&ViewMatrix, 0, 0, -2);


  SetupShader();

  ::g_pTheMeshManager = new cMeshManager();

//  CreateCube();
  //unsigned int VBO_ID = 0;
  //if ( !::g_pTheMeshManager->LoadMeshIntoVBO("tie_Unit_BBox.ply", VBO_ID) )
  //{
	 // std::cout << "Oh no." << std::endl;
	 // exit(-1);
  //}
  //// Assume we are loading these just fine, thank you very much...
  //::g_pTheMeshManager->LoadMeshIntoVBO("bun_zipper_res3_1UnitBB.ply", VBO_ID);
  //::g_pTheMeshManager->LoadMeshIntoVBO("mig29_xyz.ply", VBO_ID);
  //::g_pTheMeshManager->LoadMeshIntoVBO("Seafloor2.ply", VBO_ID);
  //// Do more magic


	// Moved to "CreateTheObjects()"

  return;
}

void InitWindow(int argc, char* argv[])
{
  glutInit(&argc, argv);
  
  glutInitContextVersion(4, 0);
  glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
  glutInitContextProfile(GLUT_CORE_PROFILE);

  glutSetOption(
    GLUT_ACTION_ON_WINDOW_CLOSE,
    GLUT_ACTION_GLUTMAINLOOP_RETURNS
  );
  
  glutInitWindowSize(CurrentWidth, CurrentHeight);

  glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);

  WindowHandle = glutCreateWindow(WINDOW_TITLE_PREFIX);

  if(WindowHandle < 1) {
    fprintf(
      stderr,
      "ERROR: Could not create a new rendering window.\n"
    );
    exit(EXIT_FAILURE);
  }
  
  glutReshapeFunc(ResizeFunction);
  glutDisplayFunc(RenderFunction);
  glutIdleFunc(IdleFunction);
  glutTimerFunc(0, TimerFunction, 0);
  glutCloseFunc(ShutErDownPeople);		//  glutCloseFunc(DestroyCube);
  // Register the keyboard callback
  glutKeyboardFunc(myKeyboardCallback);
  //glutKeyboardUpFunc()
  glutSpecialFunc(mySpecialKeyboardCallback);
}

void ResizeFunction(int Width, int Height)
{
  CurrentWidth = Width;
  CurrentHeight = Height;
  glViewport(0, 0, CurrentWidth, CurrentHeight);
  //ProjectionMatrix =
  //  CreateProjectionMatrix(
  //    60,
  //    (float)CurrentWidth / CurrentHeight,
  //    1.0f,
  //    100.0f
  //  );
  matProjection = glm::perspective(
	              // 60.0f,				// <-- not this
	              glm::radians(60.0f),  // <-- this instead
                  (float)CurrentWidth / CurrentHeight,
                  0.1f,		// Near 
                  10000.0f    // Far
   );	              

//  glUseProgram(ShaderIds[0]);
  ::g_pTheShaderManager->UseShaderProgram("basicShader");

//  glUniformMatrix4fv(ProjectionMatrixUniformLocation, 1, GL_FALSE, 
//	  ProjectionMatrix.m);
  glUniformMatrix4fv(ProjectionMatrixUniformLocation, 
	                 1, GL_FALSE, 
	                 glm::value_ptr(matProjection) );
//  glUseProgram(0);
  ::g_pTheShaderManager->UseShaderProgram(0);
}

// Will move this later... 
//cGameObject* pTheOneGO = 0;

void SetLightUniforms(void)
{
	  	for ( int index = 0; index != NUMBEROFLIGHTS; index++ )
	{
		glUniform4f( ::g_vecLights[index].UniLoc_position, 
					 ::g_vecLights[index].position.x, 
					 ::g_vecLights[index].position.y, 
					 ::g_vecLights[index].position.z, 
					 ::g_vecLights[index].position.w );
		glUniform4f( ::g_vecLights[index].UniLoc_diffuse, 
					 ::g_vecLights[index].diffuse.x, 
					 ::g_vecLights[index].diffuse.y, 
					 ::g_vecLights[index].diffuse.z, 
					 ::g_vecLights[index].diffuse.w );
		glUniform4f( ::g_vecLights[index].UniLoc_ambient, 
					 ::g_vecLights[index].ambient.x, 
					 ::g_vecLights[index].ambient.y, 
					 ::g_vecLights[index].ambient.z, 
					 ::g_vecLights[index].ambient.w );
		glUniform4f( ::g_vecLights[index].UniLoc_specular, 
					 ::g_vecLights[index].specular.x, 
					 ::g_vecLights[index].specular.y, 
					 ::g_vecLights[index].specular.z, 
					 ::g_vecLights[index].specular.w );
		glUniform1f( ::g_vecLights[index].UniLoc_attenConst, ::g_vecLights[index].attenConst);
		glUniform1f( ::g_vecLights[index].UniLoc_attenLinear, ::g_vecLights[index].attenLinear);
		glUniform1f( ::g_vecLights[index].UniLoc_attenQuad, ::g_vecLights[index].attenQuad);

		glUniform1f( ::g_vecLights[index].UniLoc_type, static_cast<float>(::g_vecLights[index].lightType) );
		
		glUniform4f( ::g_vecLights[index].UniLoc_direction, 
					 ::g_vecLights[index].direction.x, ::g_vecLights[index].direction.y,
					 ::g_vecLights[index].direction.z, ::g_vecLights[index].direction.w );
		glUniform1f( ::g_vecLights[index].UniLoc_anglePenumbraStart, ::g_vecLights[index].anglePenumbraStart );
		glUniform1f( ::g_vecLights[index].UniLoc_anglePenumbraEnd, ::g_vecLights[index].anglePenumbraEnd );
		// ... and so on.. 
	}

	ExitOnGLError("ERROR in SetUpLightUniforms()");

	return;
}

// Here's a handy function that will set the texture and binding attrubutes at once
bool SetTextureBinding( std::string texture, GLenum textureUnit, GLint samplerNumber )
{
	GLuint textureNum = 0;
	if ( ! ::g_pTheTextureManager->GetTextureNumberFromName(texture, textureNum) )
	{	// Can't find that texture in the texture manager.
		return false;
	}
	// Note that we are subtracting the define GL_TEXTURE0;
	// if you look at the defines, they are in order. Convenient, eh?
	glActiveTexture( textureUnit );					// GL_TEXTURE0, etc.
	glBindTexture( GL_TEXTURE_2D, textureNum );		// Number from glGenTexture()
	glUniform1i( UniLoc_texSampler2D[samplerNumber], textureUnit - GL_TEXTURE0 );	// 0, 1, etc.	

	//ExitOnGLError("ERROR: AssignTextureUnitSimple()");

	return true;
}

bool AssignTextureUnitsSimple(void)
{
	// This simply sets the texture units in the shader based on how they were 
	//	loaded by the texture manager.

//	::g_pTheTextureManager->UpdateTextureBindings();

	// The textures are loaded into texture "units", but they are not "bound" 
	//  to a "binding point" or to a "sampler unit" in the shader
	// 
	// So you have to do two things:
	// * Assign the texture unit the "bind point" in the GPU
	// * Assign this "bind point" to a particular "sampler unit" in the shader
	// 
	// Serious "gotcha": There are a couple ways to assign texture units, 
	//  one that has been around for a long time (and is more common), and a newer
	//  way (OpenGL 4.5 and later). The difference is the number of texture units 
	//  that are available. Pre OpenGL 4.5 had at least 16 or 32 texture unit (and often 
	//  at least 80), that have pre-defined constants: GL_TEXTURE0 through GL_TEXTURE31.
	//  (and, in fact, you can go up to 80 here, possibly)
	// Keep in mind that this is NOT the total number of textures you can load, but 
	//	simply the NUMBER OF TEXTURES YOU CAN ASSIGN TO EACH SHADER AT ANY TIME. 
	// But another way, you could load 1000s of textures (until your GPU memory is full), 
	//  but for each shader, you could only assign 16-32 AT THE SAME TIME. 
	// Huh? Bottom line: that's a TON of textures - even with a lower end (like inexpensive
	//  circa 2010 GPU), you can sample ("multi-sample") 16 textures at the same time, for 
	//  the SAME PIXEL FRAGMENT. 
	// Also keep in mind that this isn't a limitation of the SIZE of the texutre image itself;
	//  you can have quite massive textures loaded (this is the primary reason GPU memories
	//  get bigger and bigger, sometimes larger than the CPU's RAM). 
	// So it's not like you have to have only one "image" per texture; often multiple textures 
	//  are combined into one, giant texture of multiple images. 
	//  (Google search for "texture atlas" to see what I mean - so you could have 
	//   at LEAST 16 of these suckers... that's a LOT of potential images)
	//
	//	+--------------+      +-----------------+     +---------------------------+
	//	|              |      |                 |     |                           |
	//	|              |      |                 |     |                           |
	//	|              |      |                 |     |                           |
	//	|              |      |                 |     |                           |
	//	| Texture Unit |    \ |  Binding point  | /   |      Shader sampler       |
	//	|              +----->+   TEXTURE UNIT  +<----+       (in shader)         |
	//	|              |    / |                 | \   | (uinform sampler2D, etc.) |
	//	|              |      |                 |     |                           |
	//	|              |      |                 |     |                           |
	//	|              |      |                 |     |                           |
	//	|              |      |                 |     |                           |
	//	|              |      |                 |     |                           |
	//	+--------------+      +-----------------+     +---------------------------+
	//
	// Last note: One of the biggest "gotchas" is that if you only have one texture, 
	// and one texture sampler, it all defaults to zero (0). This is fine, IF you
	// only have one texture, but the minute you have MORE than one texture 
	// (and to do anything interesting, you will need more than one), you have to 
	// deal with this "indirect binding" situation. 
	

	bool bNoErrors = true;

	// Step 1: Find the "texture number" for the image:
	GLuint textureNum_Brick = 0;
	if ( ! ::g_pTheTextureManager->GetTextureNumberFromName("glass.bmp", textureNum_Brick) )
	{
		bNoErrors = false;
	}


	// Step 2: Assign this texture to a "binding point" in the GPU
	// Pre OpenGL 4.5, these are given defines from GL_TEXTURE0 to GL_TEXTURE31
	// We'll assign it to GL_TEXTURE0, just because. 
	// 33,984
	glActiveTexture( GL_TEXTURE0 );		// make this texture unit "active"
	// From this point on, anything "texture stuff" we do is assigned to the "active" texture
	// (which is a little indirect, but that's the way it is...)
	ExitOnGLError("ERROR: AssignTextureUnitSimple()");

	// Step 3: Assign this texture (the "active" one) to a "binding point".
	// Note: we also have to be careful that the TYPE of texture is consistent. 
	//  In this case, the texture is a 2D texture, so we bind it as a 2D texture. 
	//  (If you don't do this, at best nothing will happen, at worst, your driver will crash)
	// Note: these are also assigned the defines, so GL_TEXTURE0 in this case
	glBindTexture( GL_TEXTURE_2D, textureNum_Brick );
	ExitOnGLError("ERROR: AssignTextureUnitSimple()");

	// Step 4: Now we tie the "binding point" to the shader "sampler"
	// Here's where it gets annoying. While the texture units and binding points 
	//  are assigned pre-defined values, the samplers are assigned values from 0 and up. 
	// So: GL_TEXTURE0 is given the value 0
	//     GL_TEXTURE1 is given the value 1
	//    ...and so on.
	// In fact, you can actually use values of GL_TEXTURE0 + whateverOffsetYouWant
	//  in the previous calls - that's why, even though the maximum define is GL_TEXTURE31, 
	//  you can still assign 80 texture uints, by doing (GL_TEXTURE0 + 80), as long as 
	//  your GPU can handle that, of course. 
	// 
	// SERIOUS GOTCHA!!! Note we are passing zero (0) NOT GL_TEXTURE0 here....
	glUniform1i( UniLoc_texSampler2D[0], 0 );   // --NOT-- GL_TEXTURE0!!!!
	ExitOnGLError("ERROR: AssignTextureUnitSimple()");


	// Now we'll do it for the the other textures...

	{
		// Place the "blue tile" in binding point 1, and sampler 1
		GLuint textureNum_BlueTile = 0;
		if ( ! ::g_pTheTextureManager->GetTextureNumberFromName("sand.bmp", textureNum_BlueTile) )
		{
			bNoErrors = false;
		}
		glActiveTexture( GL_TEXTURE1 );		// make this texture unit "active"
		glBindTexture( GL_TEXTURE_2D, textureNum_BlueTile );
		glUniform1i( UniLoc_texSampler2D[1], 1 );		// Note the "1" instead of GL_TEXTURE1
	}

	{
		// Place the "Gold" in binding point 2, and sampler 2
		GLuint textureNum_Gold = 0;
		if ( ! ::g_pTheTextureManager->GetTextureNumberFromName("rock19tex_0.bmp", textureNum_Gold) )
		{
			bNoErrors = false;
		}
		glActiveTexture( GL_TEXTURE2 );		// make this texture unit "active"
		glBindTexture( GL_TEXTURE_2D, textureNum_Gold );
		glUniform1i( UniLoc_texSampler2D[2], 2 );		// Note the "2" instead of GL_TEXTURE2
	}

	{
		// Place the "Blue whale" in binding point 3, and sampler 3
		GLuint textureNum_BlueWhale = 0;
		if ( ! ::g_pTheTextureManager->GetTextureNumberFromName("Vegetation_Blur7.bmp", textureNum_BlueWhale) )
		{
			bNoErrors = false;
		}
		glActiveTexture( GL_TEXTURE3 );		// make this texture unit "active"
		glBindTexture( GL_TEXTURE_2D, textureNum_BlueWhale );
		glUniform1i( UniLoc_texSampler2D[3], 3 );		// Note the "3" instead of GL_TEXTURE3
	}

	// Or something like this...
	SetTextureBinding( "rock1.bmp", GL_TEXTURE0, 0 );
	SetTextureBinding( "rock2.bmp", GL_TEXTURE1, 1 );
	SetTextureBinding( "rock3.bmp", GL_TEXTURE2, 2 );
	SetTextureBinding( "glass.bmp", GL_TEXTURE3, 3 );
	// Set the rest to pure white... Why?? We'll see, soon
	SetTextureBinding( "stars.bmp", GL_TEXTURE4, 4 );

	SetTextureBinding( "TropicalFish02.bmp", GL_TEXTURE5, 5 );
	SetTextureBinding( "TropicalFish03.bmp", GL_TEXTURE6, 6 );

	//	SetTextureBinding( "WhiteSquare128x128.bmp", GL_TEXTURE7, 7 );
	//SetTextureBinding( "Fence_Mask.bmp", GL_TEXTURE7, 7 );
	SetTextureBinding( "popcan.bmp", GL_TEXTURE7, 7 );			// <-- "Mask" texture
	//SetTextureBinding( "13982137-Fiery-Explosion-Seamless-Texture-Tile-Stock-Photo.bmp", GL_TEXTURE6, 6 );	// "explosion" texture

	return bNoErrors;
}

void RenderFunction(void)
{


	++FrameCount;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Set up a camera... 

	matView = glm::mat4(1.0f);

	if(g_vec_pGOs.size() > 1)
	::g_cam_at = ::g_vec_pGOs[g_selectedObject]->position;

	//matView = glm::lookAt(glm::vec3(0.0f, 0.0f, 10.0f),  // "eye"
		//                   glm::vec3(0.0f, 0.0f, 0.0f),   // "At"
		//                   glm::vec3(0.0f, 1.0f, 0.0f));  // up
	matView = glm::lookAt( ::g_cam_eye,  // "eye"
							::g_cam_at,   // "At"
						glm::vec3(0.0f, 1.0f, 0.0f));  // up

	::g_pTheShaderManager->UseShaderProgram("basicShader");

	glUniform3f( UniLoc_eye, ::g_cam_eye.x, ::g_cam_eye.y, ::g_cam_eye.z );

	SetLightUniforms();

	AssignTextureUnitsSimple();
	ExitOnGLError("ERROR: Could not set the shader uniforms");


	//glUniform4f( UniLoc_Light_0_position, 0.0f, 3.0f, 0.0f, 1.0f );	// "theLights[0].position"); 
	//glUniform4f( UniLoc_Light_0_ambient, 0.2f, 0.0f, 0.02f, 1.0f );	// "theLights[0].ambient");
	//glUniform4f( UniLoc_Light_0_diffuse, 1.0f, 0.0f, 0.0f, 1.0f );	// "theLights[0].diffuse");  
	//glUniform4f( UniLoc_Light_0_specular, 1.0f, 1.0f, 1.0f, 1.0f );	// "theLights[0].specular");  
	//glUniform1f( UniLoc_Light_0_attenConst, 0.1f );	// "theLights[0].attenConst");  
	//glUniform1f( UniLoc_Light_0_attenLinear, 0.1f );	// "theLights[0].attenLinear");  
	//glUniform1f( UniLoc_Light_0_attenQuad, 0.1f );	// "theLights[0].attenQuad");  
	//
	//glUniform4f( UniLoc_Light_1_position, 3.0f, 3.0f, 0.0f, 1.0f );	// "theLights[0].position"); 
	//glUniform4f( UniLoc_Light_1_ambient, 0.0f, 0.2f, 0.0f, 1.0f );	// "theLights[0].ambient");
	//glUniform4f( UniLoc_Light_1_diffuse, 0.0f, 1.0f, 0.0f, 1.0f );	// "theLights[0].diffuse");  
	//glUniform4f( UniLoc_Light_1_specular, 1.0f, 1.0f, 1.0f, 1.0f );	// "theLights[0].specular");  
	//glUniform1f( UniLoc_Light_1_attenConst, 0.1f );	// "theLights[0].attenConst");  
	//glUniform1f( UniLoc_Light_1_attenLinear, 0.1f );	// "theLights[0].attenLinear");  
	//glUniform1f( UniLoc_Light_1_attenQuad, 0.1f );	// "theLights[0].attenQuad");  

	// Put light where tie fighter is...
	//::g_vecLights[1].position.x = ::g_vec_pGOs[0]->position.x - 1.0f;
	//::g_vecLights[1].position.y = ::g_vec_pGOs[0]->position.y;
	//::g_vecLights[1].position.z = ::g_vec_pGOs[0]->position.z;


	g_vecLights[1].position = glm::vec4(g_vec_pGOs[11]->position.x, g_vec_pGOs[11]->position.y, g_vec_pGOs[11]->position.z, 1.0f);
	g_vecLights[5].position = glm::vec4(g_vec_pGOs[12]->position.x, g_vec_pGOs[12]->position.y, g_vec_pGOs[12]->position.z, 1.0f);
	//if bullets have not reached falcon yet
	if (g_vec_pGOs[11]->position.x < g_vec_pGOs[11]->position.x)
	{
		g_vec_pGOs[11]->position.x += 0.005;
	}
	if ((g_vec_pGOs[11]->position.y > g_vec_pGOs[11]->position.y))
	{
		g_vec_pGOs[11]->position.y -= 0.005;
	}
	if (g_vec_pGOs[11]->position.z > g_vec_pGOs[11]->position.z)
	{
		g_vec_pGOs[11]->position.z -= 0.005;
	}

	if (g_vec_pGOs[12]->position.x < g_vec_pGOs[11]->position.x)
	{
		g_vec_pGOs[12]->position.x += 0.005;
	}
	if ((g_vec_pGOs[12]->position.y > g_vec_pGOs[11]->position.y))
	{
		g_vec_pGOs[12]->position.y -= 0.005;
	}
	if (g_vec_pGOs[12]->position.z > g_vec_pGOs[11]->position.z)
	{
		g_vec_pGOs[12]->position.z -= 0.005;
	}

	for (std::vector< cGameObject* >::iterator itGO = ::g_vec_pGOs.begin();
		itGO != ::g_vec_pGOs.end(); itGO++)
	{
		cGameObject* pCurGO = *itGO;
		DrawObject(pCurGO);
	}

	if ( g_bDebugLights )
	{
		// White, tiny ball at position
		::g_pDebugBall->position = glm::vec3(::g_vecLights[g_selectedLightIndex].position.x,
												::g_vecLights[g_selectedLightIndex].position.y, 
												::g_vecLights[g_selectedLightIndex].position.z);
		::g_pDebugBall->debugColour = glm::vec4(1.0f, 1.0f, 0.0f, 1.0f);
		::g_pDebugBall->bUseDebugColour = true;
		::g_pDebugBall->scale = 0.1f;
		DrawObject(::g_pDebugBall);

		// At 75% brightness
		::g_pDebugBall->debugColour = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
		::g_pDebugBall->bUseDebugColour = true;
		::g_pDebugBall->scale 
			= ::g_vecLights[g_selectedLightIndex].calcDistanceAtBrightness(0.75f);
		DrawObject(::g_pDebugBall);

		// at 50% brightness
		::g_pDebugBall->debugColour = glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
		::g_pDebugBall->scale 
			= ::g_vecLights[g_selectedLightIndex].calcDistanceAtBrightness(0.50f);
		DrawObject(::g_pDebugBall);

		// at 25% 
		::g_pDebugBall->scale = 1.0f/ 0.01f;		// quad atten
		::g_pDebugBall->debugColour = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
		::g_pDebugBall->scale 
			= ::g_vecLights[g_selectedLightIndex].calcDistanceAtBrightness(0.25f);
		DrawObject(::g_pDebugBall);
	}
  
	glutSwapBuffers();
}

//*********************************************************************
//Needs bool g_bCtrlPressed
//Lights vector is populated in SetupShader and calls numLights from shader
//Movement is in HandleIO  and object and light selection is in the KeyboardCallback
//To make it functional it needs a global variable called g_selectedObject
//and g_selectedLightIndex
//*********************************************************************
void HandleIO(void)
{
	// Reset Object velocity so it doesn't keep flying
	if (g_vec_pGOs.size() > 0)
		::g_vec_pGOs[g_selectedObject]->velocity = glm::vec3(0.0f);


	// Thruster object moves with accel
	//::g_vec_pGOs[g_selectedObject]->accel = glm::vec3(0.0f);
	//::g_vec_pGOs[g_selectedObject]->velocity *= 0.99f;	

	//ATTENUATION of selected lights change
	if ((GetAsyncKeyState('1') & 0x8000) != 0)
	{	// Increase attenuation (making it darker)
		::g_vecLights[::g_selectedLightIndex].attenLinear *= 1.01f;
	}
	if ((GetAsyncKeyState('2') & 0x8000) != 0)
	{	// Decrease attenuation (making it darker)
		::g_vecLights[::g_selectedLightIndex].attenLinear *= 0.99f;
	}
	if ((GetAsyncKeyState('3') & 0x8000) != 0)
	{	// Increase attenuation (making it darker)
		::g_vecLights[::g_selectedLightIndex].attenQuad *= 1.001f;
	}
	if ((GetAsyncKeyState('4') & 0x8000) != 0)
	{	// Decrease attenuation (making it darker)
		::g_vecLights[::g_selectedLightIndex].attenQuad *= 0.999f;
	}

	//If ENTER is pressed commands can be entered to perform various file read write functions
	if ((GetAsyncKeyState('C') & 0x8000))
	{
		bool bFinish = false;
		std::string strCommand;
		do
		{
			std::cout << "Please enter a command (/help for commands): ";
			std::cin >> strCommand;
			if (strCommand == "/help")
			{
				std::cout << "Available commands:" << std::endl;
				std::cout << "/exit + ctrl + enter		to return" << std::endl;
				std::cout << "/LoadFile filePath		loads objects in a file" << std::endl;
				std::cout << "/SaveFile filePath		saves the current configuration to a txt file" << std::endl;
				std::cout << "/CreateObject	plyName		creates an object with default values at the origin from the specified ply" << std::endl;
				std::cout << "/CreateLight				creates a light at the origin with default values in the index after selected light(only if max lights has not been reached " << "Current max:" << NUMBEROFLIGHTS << std::endl;
				std::cout << "/CopyObject				copies the currently selected object" << std::endl;
				std::cout << "/ClearObject				deletes the currently selected objects" << std::endl;
				std::cout << "/ClearLight				deletes the currently selected light" << std::endl;
				std::cout << "/ClearAll					deletes all objects and lights" << std::endl;
				std::cout << "/SetTexture				ON or OFF" << std::endl;
				std::cout << "/ClearTextureMix			clears values so textures can be applied" << std::endl;
				
				std::cout << "/SetAlpha					ON or OFF" << std::endl;
				std::cout << "/SetVertexRGBA			ON or OFF" << std::endl;
				std::cout << "/SetDiscardMask			ON or OFF" << std::endl;//Can change in the shader where if checks if bUseDiscardMask currently uses the flash
				std::cout << "/SetTextureMix			change mix takes in index and mix as float 0-1" << std::endl;
				std::cout << "/SetAlphaVal				takes in a float for transparency ratio" << std::endl;


				std::cout << std::endl << "Keyboard Commands:" << std::endl;
				std::cout << "WASD" << std::endl << "Moves along x and z plane" << std::endl;
				std::cout << "QE" << std::endl << "Moves along y plane" << std::endl;
				std::cout << std::endl << "1,2" << std::endl << "Changes linear attenuation of selected light" << std::endl;
				std::cout << "3,4" << std::endl << "Changes quadratic attenuation of selected light" << std::endl;
				std::cout << std::endl << "(shift)space" << std::endl << "Adjusts scale of selected object" << std::endl;
				std::cout << std::endl << "(R) or (T) x,y,z" << std::endl << "Adjusts rotation of selected object" << std::endl;
				std::cout << std::endl << "ctrl" << std::endl << "Toggles between selection of light and object for movement" << std::endl;
				std::cout << "+,-" << std::endl << "Cycles through index of objects or lights (depends on ctrl toggle)" << std::endl;
				std::cout << std::endl << "( or )" << std::endl << "Toggle Light debugging on or off" << std::endl << std::endl;
			}
			if (strCommand == "/exit")
			{
				bFinish = true;
			}
			else if (strCommand == "/LoadFile")
			{
				std::cin >> strCommand;
				//read path to load file
				std::string path;
				path = strCommand;
				readFile(path);
			}
			else if (strCommand == "/SaveFile")
			{
				std::cin >> strCommand;
				//read path to load file
				std::string path;
				path = strCommand;
				writeFile(path);
			}
			else if (strCommand == "/CreateObject")
			{
				std::cin >> strCommand;
				//load an object of that ply
				createObject(strCommand);
			}
			else if (strCommand == "/CopyObject")
			{
				//load an object of that ply
				std::cout << g_vec_pGOs.size() << std::endl;
				cGameObject* temp = g_vec_pGOs[g_selectedObject];
				g_vec_pGOs.push_back(temp);
				std::cout << g_vec_pGOs.size() << std::endl;

			}
			else if (strCommand == "/CreateLight")
			{
				int lightType;
				std::cin >> lightType;

				if (g_selectedLightIndex + 1 < NUMBEROFLIGHTS - 1)
				{
					//load an object of that ply
					createLight(g_selectedLightIndex + 1, lightType);
					g_selectedLightIndex = 0;
				}
			}
			else if (strCommand == "/ClearObject")
			{
				if (g_selectedObject >= 0 && g_selectedObject < g_vec_pGOs.size())
				{
					//remove object from vector at selected index
					g_vec_pGOs.erase(g_vec_pGOs.begin() + g_selectedObject);
					g_selectedObject = 0;
					std::cout << "Object deleted" << g_vec_pGOs.size() << std::endl;

				}
			}
			else if (strCommand == "/ClearLight")
			{
				if (g_selectedLightIndex >= 0 && g_selectedLightIndex < g_vecLights.size())
				{
					::g_vecLights[g_selectedLightIndex].position = defaultValue04f;
					::g_vecLights[g_selectedLightIndex].diffuse = defaultValue04f;
					::g_vecLights[g_selectedLightIndex].ambient = defaultValue04f;
					::g_vecLights[g_selectedLightIndex].specular = defaultValue04f;
					::g_vecLights[g_selectedLightIndex].attenConst = 0.0f;
					::g_vecLights[g_selectedLightIndex].attenLinear = 0.0f;
					::g_vecLights[g_selectedLightIndex].attenQuad = 0.0f;
				}
			}
			else if (strCommand == "/ClearAll")
			{
				for (unsigned i = 0; i < g_vec_pGOs.size(); ++i)
				{
					//remove object from vector at selected index
					g_vec_pGOs.erase(g_vec_pGOs.begin(), g_vec_pGOs.end());
					std::cout << "All lights and objects cleared" << g_vec_pGOs.size() << std::endl;
				}
				for (unsigned i = 0; i < g_vecLights.size(); ++i)
				{
					::g_vecLights[i].position = defaultValue04f;
					::g_vecLights[i].diffuse = defaultValue04f;
					::g_vecLights[i].ambient = defaultValue04f;
					::g_vecLights[i].specular = defaultValue04f;
					::g_vecLights[i].attenConst = 0.0f;
					::g_vecLights[i].attenLinear = 0.0f;
					::g_vecLights[i].attenQuad = 0.0f;
				}
			}
			else if (strCommand == "/SetTexture")
			{
				std::cin >> strCommand;
				if (strCommand == "ON")
					g_vec_pGOs[g_selectedObject]->bUseTexturesAsMaterials = true;
				else if (strCommand == "OFF")
					g_vec_pGOs[g_selectedObject]->bUseTexturesAsMaterials = false;

			}
			else if (strCommand == "/ClearTextureMix")
			{
				g_vec_pGOs[g_selectedObject]->ClearTextureMixValues(NUMBEROF2DSAMPLERS, 0.0f);
			}
			else if (strCommand == "/SetVertexRGBA")
			{
				std::cin >> strCommand;
				if (strCommand == "ON")
					g_vec_pGOs[g_selectedObject]->bUseVertexRGBAColoursAsMaterials = true;
				else if (strCommand == "OFF")
					g_vec_pGOs[g_selectedObject]->bUseVertexRGBAColoursAsMaterials = false;
			}
			else if (strCommand == "/SetAlpha")
			{
				std::cin >> strCommand;
				if (strCommand == "ON")
					g_vec_pGOs[g_selectedObject]->bIsEntirelyTransparent = true;
				else if (strCommand == "OFF")
					g_vec_pGOs[g_selectedObject]->bIsEntirelyTransparent = false;
			}
			else if (strCommand == "/SetAlphaVal")
			{
				float alphaVal;
				std::cin >> alphaVal;
				g_vec_pGOs[g_selectedObject]->alphaValue = alphaVal;
			}
			else if (strCommand == "/SetDiscardMask")
			{
				std::cin >> strCommand;
				if (strCommand == "ON")
					g_vec_pGOs[g_selectedObject]->bUseDiscardMask = true;
				else if (strCommand == "OFF")
					g_vec_pGOs[g_selectedObject]->bUseDiscardMask = false;

			}
			else if (strCommand == "/SetTextureMix")
			{
				int index;
				std::cin >> index;
				float blendValue;
				std::cin >> blendValue;
				if (index < g_vec_pGOs[g_selectedObject]->vecTextureMixRatios.size() && index >= 0)
					g_vec_pGOs[g_selectedObject]->vecTextureMixRatios[index] = blendValue;
				else
					std::cout << "Invalid Index specified" << std::endl;

			}
		} while (bFinish == false);//end while not finished
	}//end if c pressed

	const float lightMoveSpeed = 0.03f;

	if (g_bCtrlPressed)
	{

		//Light Movement
		if ((GetAsyncKeyState('A') & 0x8000) != 0)
		{
			::g_vecLights[::g_selectedLightIndex].position.x -= lightMoveSpeed;
		}
		if ((GetAsyncKeyState('D') & 0x8000) != 0)
		{
			::g_vecLights[::g_selectedLightIndex].position.x += lightMoveSpeed;
		}
		if ((GetAsyncKeyState('W') & 0x8000) != 0)
		{
			::g_vecLights[::g_selectedLightIndex].position.z += lightMoveSpeed;
		}
		if ((GetAsyncKeyState('S') & 0x8000) != 0)
		{
			::g_vecLights[::g_selectedLightIndex].position.z -= lightMoveSpeed;
		}
		if ((GetAsyncKeyState('Q') & 0x8000) != 0)
		{
			::g_vecLights[::g_selectedLightIndex].position.y += lightMoveSpeed;
		}
		if ((GetAsyncKeyState('E') & 0x8000) != 0)
		{
			::g_vecLights[::g_selectedLightIndex].position.y -= lightMoveSpeed;
		}
	}
	else
	{	// Move object 
		if ((GetAsyncKeyState(VK_SPACE) & 0x8000))
		{
			if ((GetAsyncKeyState(VK_LSHIFT) & 0x8000) || (GetAsyncKeyState(VK_RSHIFT) & 0x8000))
			{	// Scale up
				::g_vec_pGOs[g_selectedObject]->scale *= 1.01;
			}
			else
			{	// Scale down
				::g_vec_pGOs[g_selectedObject]->scale *= 0.99;
			}
		}

		if ((GetAsyncKeyState('A') & 0x8000) != 0)
		{	// Move object Left
			::g_vec_pGOs[g_selectedObject]->velocity.x = -2.0f;
		}
		if ((GetAsyncKeyState('D') & 0x8000) != 0)
		{	// Move object Right
			::g_vec_pGOs[g_selectedObject]->velocity.x = +2.0f;
		}
		if ((GetAsyncKeyState('W') & 0x8000) != 0)
		{
			::g_vec_pGOs[g_selectedObject]->velocity.z = +2.0f;
		}
		if ((GetAsyncKeyState('S') & 0x8000) != 0)
		{
			::g_vec_pGOs[g_selectedObject]->velocity.z = -2.0f;
		}
		if ((GetAsyncKeyState('Q') & 0x8000) != 0)
		{
			::g_vec_pGOs[g_selectedObject]->velocity.y = +2.0f;
		}
		if ((GetAsyncKeyState('E') & 0x8000) != 0)
		{
			::g_vec_pGOs[g_selectedObject]->velocity.y = -2.0f;
		}
		//Object rotation Press R forward rotation and then the axis to rotate around
		float rotPerCycle = 0.5f;
		bool bRPressed = (GetAsyncKeyState('R') & 0x8000) || (GetAsyncKeyState('r') & 0x8000);
		if (bRPressed)
		{
			if ((GetAsyncKeyState('X') & 0x8000) != 0)
			{
				::g_vec_pGOs[g_selectedObject]->preRotation.x += glm::radians(rotPerCycle);
			}
			if ((GetAsyncKeyState('Y') & 0x8000) != 0)
			{
				::g_vec_pGOs[g_selectedObject]->preRotation.y += glm::radians(rotPerCycle);
			}
			if ((GetAsyncKeyState('Z') & 0x8000) != 0)
			{
				::g_vec_pGOs[g_selectedObject]->preRotation.z += glm::radians(rotPerCycle);
			}
		}
		//Object rotation Press T reverse rotation and then the axis to rotate around
		bool bTPressed = (GetAsyncKeyState('T') & 0x8000) || (GetAsyncKeyState('t') & 0x8000);
		if (bTPressed)
		{
			if ((GetAsyncKeyState('X') & 0x8000) != 0)
			{
				::g_vec_pGOs[g_selectedObject]->preRotation.x -= glm::radians(rotPerCycle);
			}
			if ((GetAsyncKeyState('Y') & 0x8000) != 0)
			{
				::g_vec_pGOs[g_selectedObject]->preRotation.y -= glm::radians(rotPerCycle);
			}
			if ((GetAsyncKeyState('Z') & 0x8000) != 0)
			{
				::g_vec_pGOs[g_selectedObject]->preRotation.z -= glm::radians(rotPerCycle);
			}
		}
	}  // if ( bCrtlPressed ) (Light or Object)
	return;
}



void IdleFunction(void)
{
	//// Look at the bunny
	//::g_cam_at = ::g_vec_pGOs[0]->position;
	//SHORT keyState_A = GetAsyncKeyState( 'A' );	
//
	//if ( ( keyState_A & 0x8000) != 0 )
	//{	// 'A' Key is down, yo
	//	std::cout << "A is down" << std::endl;
	//}
	//else
	//{
	//	std::cout << "A is up" << std::endl;
	//}

	HandleIO();

	// Get elapsed secs since last idle (likely 0.1 ms)
	float deltaTime = g_AniTimer.GetElapsedSeconds( true );

	// This is technically "Explicit forward Euler integration"
	cGameObject* pCurGO = 0;
	for ( std::vector< cGameObject* >::iterator itGO = ::g_vec_pGOs.begin();
		  itGO != ::g_vec_pGOs.end(); itGO++ )
	{
		pCurGO = *itGO;

		// Doing x, y, and z at the same time
		pCurGO->velocity += pCurGO->accel * deltaTime;		// 1.0;

		pCurGO->position += pCurGO->velocity * deltaTime; 

		//pCurGO->velocity.x += pCurGO->accel.x * deltaTime;
		//pCurGO->velocity.y += pCurGO->accel.y * deltaTime;
		//pCurGO->velocity.z += pCurGO->accel.z * deltaTime;
	}

	glutPostRedisplay();
}

void TimerFunction(int Value)
{
  //if (0 != Value) {
  //  char* TempString = (char*)
  //    malloc(512 + strlen(WINDOW_TITLE_PREFIX));
//
  //  sprintf(
  //    TempString,
  //    "%s: %d Frames Per Second @ %d x %d",
  //    WINDOW_TITLE_PREFIX,
  //    FrameCount * 4,
  //    CurrentWidth,
  //    CurrentHeight
  //  );

	if (g_vecLights.size() != 0 && g_vec_pGOs.size() != 0)
	{
		std::stringstream ssTitle;
		ssTitle << std::fixed << std::setprecision(3)
			<< "Object# (" << g_selectedObject << ") Pos:"
			<< ::g_vec_pGOs[::g_selectedObject]->position.x
			<< ", " << ::g_vec_pGOs[::g_selectedObject]->position.y
			<< ", " << ::g_vec_pGOs[::g_selectedObject]->position.z
			<< " Light# (" << ::g_selectedLightIndex << ") Pos:"
			<< ::g_vecLights[::g_selectedLightIndex].position.x
			<< ", " << ::g_vecLights[::g_selectedLightIndex].position.y
			<< ", " << ::g_vecLights[::g_selectedLightIndex].position.z
			<< " Linear: "
			<< ::g_vecLights[::g_selectedLightIndex].attenLinear
			<< " Quad: "
			<< ::g_vecLights[::g_selectedLightIndex].attenQuad;

		glutSetWindowTitle(ssTitle.str().c_str());
	}

    //glutSetWindowTitle(TempString);
    //free(TempString);

  //FrameCount = 0;
  glutTimerFunc(100, TimerFunction, 1);
}



bool bCheckShaderCompileStatus(GLuint shaderID, std::string errors)
{
	GLint statusOK; 
	::glGetObjectParameterivARB( shaderID , GL_OBJECT_COMPILE_STATUS_ARB, &statusOK );
	if ( !statusOK )
	{
		// This gets the 'last' error message for that shader (if there was one)
		GLcharARB infoLog[ GL_INFO_LOG_LENGTH ];	// defined in glext.h
		glGetInfoLogARB( shaderID , GL_INFO_LOG_LENGTH, NULL, infoLog );
		std::stringstream ss;
		ss << infoLog << std::endl;
		errors = ss.str();
		return false;
	}
	// No errors
	return true;
}

bool bCheckShaderLinkStatus(GLuint shaderID, std::string errors)
{
	GLint statusOK; 
	::glGetObjectParameterivARB( shaderID , GL_LINK_STATUS, &statusOK );
	if ( !statusOK )
	{
		// This gets the 'last' error message for that shader (if there was one)
		GLcharARB infoLog[ GL_INFO_LOG_LENGTH ];	// defined in glext.h
		glGetInfoLogARB( shaderID , GL_INFO_LOG_LENGTH, NULL, infoLog );
		std::stringstream ss;
		ss << infoLog << std::endl;
		errors = ss.str();
		return false;
	}
	// No errors
	return true;
}

GLint getLightUniformLocation( int shaderID, int index, std::string attribute )
{
	std::stringstream ssUniformName;
	ssUniformName << "theLights[" << index << "]." << attribute;
	return glGetUniformLocation(shaderID, ssUniformName.str().c_str() );
}

void SetUpUniformVariables(void)
{
	GLuint shaderID = ::g_pTheShaderManager->GetShaderIDFromName("basicShader");

	ModelMatrixUniformLocation
		= glGetUniformLocation(shaderID /*ShaderIds[0]*/, "ModelMatrix");

	ViewMatrixUniformLocation
		= glGetUniformLocation(shaderID /*ShaderIds[0]*/, "ViewMatrix");

	ProjectionMatrixUniformLocation
		= glGetUniformLocation(shaderID /*ShaderIds[0]*/, "ProjectionMatrix");

	ModelMatrixRotationOnlyUniformLocation
		= glGetUniformLocation(shaderID, "ModelMatrixRotOnly");


	UniLoc_MaterialAmbient_RGB = glGetUniformLocation(shaderID, "myMaterialAmbient_RGB");
	UniLoc_MaterialDiffuse_RGB = glGetUniformLocation(shaderID, "myMaterialDiffuse_RGB");
	UniLoc_MaterialSpecular = glGetUniformLocation(shaderID, "myMaterialSpecular"); 
	UniLoc_MaterialShininess = glGetUniformLocation(shaderID, "myMaterialShininess"); 

	UniLoc_eye = glGetUniformLocation(shaderID, "eye"); 

	UniLoc_debugColour = glGetUniformLocation(shaderID,  "debugColour");
	UniLoc_bUseDebugColour = glGetUniformLocation(shaderID,  "bUseDebugColour");
	UniLoc_bUseVertexRGBAColours = glGetUniformLocation(shaderID,  "bUseVertexRGBAColours");
	UniLoc_bUseTextureMaterials = glGetUniformLocation(shaderID,  "bUseTextureMaterials");
	UniLoc_bUseTexturesOnly = glGetUniformLocation(shaderID, "bUseTexturesNoLighting");

	UniLoc_myAlphaAllObject = glGetUniformLocation(shaderID, "myAlphaAllObject" );
	UniLoc_bAlphaForEntireObject = glGetUniformLocation(shaderID, "bAlphaForEntireObject" );

	UniLoc_bUseDiscardMask =  glGetUniformLocation(shaderID, "bUseDiscardMask" );

	for ( int index = 0; index != NUMBEROFLIGHTS; index++ )
	{
		cLightDesc curLight;
		
		//{
		//	std::stringstream ssPosition;
		//	ssPosition << "theLights[" << index << "].position";
		//	//UniLoc_Light_0_position = glGetUniformLocation(shaderID, "theLights[0].position"); 
		//	curLight.UniLoc_position = glGetUniformLocation(shaderID, ssPosition.str().c_str() );
		//}
		//{
		//	std::stringstream ssAmbient;		
		//	ssAmbient << "theLights[" << index << "].ambient";
		//	curLight.UniLoc_ambient = glGetUniformLocation(shaderID, ssAmbient.str().c_str() );
		//}
		//{
		//	std::stringstream ssDiffuse;		
		//	ssDiffuse << "theLights[" << index << "].diffuse";
		//	curLight.UniLoc_diffuse = glGetUniformLocation(shaderID, ssDiffuse.str().c_str() );
		//}
		//{
		//	std::stringstream ssSpecular;		
		//	ssSpecular << "theLights[" << index << "].specular";
		//	curLight.UniLoc_specular = glGetUniformLocation(shaderID, ssSpecular.str().c_str() );
		//}
		//{
		//	std::stringstream ssAttenConst;		
		//	ssAttenConst << "theLights[" << index << "].attenConst";
		//	curLight.UniLoc_attenConst = glGetUniformLocation(shaderID, ssAttenConst.str().c_str() );
		//}
		//{
		//	std::stringstream ssAttenLinear;		
		//	ssAttenLinear << "theLights[" << index << "].attenLinear";
		//	curLight.UniLoc_attenLinear = glGetUniformLocation(shaderID, ssAttenLinear.str().c_str() );
		//}
		//{
		//	std::stringstream ssAttenQuad;		
		//	ssAttenQuad << "theLights[" << index << "].attenQuad";
		//	curLight.UniLoc_attenQuad = glGetUniformLocation(shaderID, ssAttenQuad.str().c_str() );
		//}

		curLight.UniLoc_position = getLightUniformLocation( shaderID, index, "position" );
		curLight.UniLoc_ambient = getLightUniformLocation( shaderID, index, "ambient" );
		curLight.UniLoc_diffuse = getLightUniformLocation( shaderID, index, "diffuse" );
		curLight.UniLoc_specular = getLightUniformLocation( shaderID, index, "specular" );
		curLight.UniLoc_attenConst = getLightUniformLocation( shaderID, index, "attenConst" );
		curLight.UniLoc_attenLinear = getLightUniformLocation( shaderID, index, "attenLinear" );
		curLight.UniLoc_attenQuad = getLightUniformLocation( shaderID, index, "attenQuad" );

		curLight.UniLoc_type = getLightUniformLocation( shaderID, index, "lightType" );

		// For the spot and directional lights
		curLight.UniLoc_direction = getLightUniformLocation( shaderID, index, "direction" );
		// For the spot lights
		curLight.UniLoc_anglePenumbraStart = getLightUniformLocation( shaderID, index, "anglePenumbraStart" );
		curLight.UniLoc_anglePenumbraEnd = getLightUniformLocation( shaderID, index, "anglePenumbraEnd" );
		
		::g_vecLights.push_back( curLight );
	}


 /*

	UniLoc_Light_0_attenQuad = glGetUniformLocation(shaderID, "theLights[0].attenQuad");  

	UniLoc_Light_1_position = glGetUniformLocation(shaderID, "theLights[1].position"); 
	UniLoc_Light_1_ambient = glGetUniformLocation(shaderID, "theLights[1].ambient");
	UniLoc_Light_1_diffuse = glGetUniformLocation(shaderID, "theLights[1].diffuse");  
	UniLoc_Light_1_specular = glGetUniformLocation(shaderID, "theLights[1].specular");  
	UniLoc_Light_1_attenConst = glGetUniformLocation(shaderID, "theLights[1].attenConst");  
	UniLoc_Light_1_attenLinear = glGetUniformLocation(shaderID, "theLights[1].attenLinear");  
	UniLoc_Light_1_attenQuad = glGetUniformLocation(shaderID, "theLights[1].attenQuad");  */


	ExitOnGLError("ERROR in SetUpUniformVariables(): Could not get shader uniform locations");

	return;
}

bool SetUpTextures(void)
{
	::g_pTheTextureManager = new CTextureManager();

	bool bItsAllGoodMan = true;

	::g_pTheTextureManager->setBasePath("assets/textures");
	
	if ( ! ::g_pTheTextureManager->Create2DTextureFromBMPFile("rock1.bmp", true) )
	{
		std::cout << "Couldn't load texture." << std::endl;
		bItsAllGoodMan = false;
	}

	if ( ! ::g_pTheTextureManager->Create2DTextureFromBMPFile("rock2.bmp", true) )
	{
		std::cout << "Couldn't load texture." << std::endl;
		bItsAllGoodMan = false;
	}
	if ( ! ::g_pTheTextureManager->Create2DTextureFromBMPFile("rock3.bmp", true) )
	{
		std::cout << "Couldn't load texture." << std::endl;
		bItsAllGoodMan = false;
	}

	if ( ! ::g_pTheTextureManager->Create2DTextureFromBMPFile("glass.bmp", true) )
	{
		std::cout << "Couldn't load texture." << std::endl;
		bItsAllGoodMan = false;
	}

	if ( ! ::g_pTheTextureManager->Create2DTextureFromBMPFile("stars.bmp", true) )
	{
		std::cout << "Couldn't load texture." << std::endl;
		bItsAllGoodMan = false;
	}

	// These are pure white and black textures.
	// It may seem pointless to load these, but bear with me... 
	if ( ! ::g_pTheTextureManager->Create2DTextureFromBMPFile("TropicalFish02.bmp", true) )
	{
		std::cout << "Couldn't load texture." << std::endl;
		bItsAllGoodMan = false;
	}
	if ( ! ::g_pTheTextureManager->Create2DTextureFromBMPFile("TropicalFish03.bmp", true) )
	{
		std::cout << "Couldn't load texture." << std::endl;
		bItsAllGoodMan = false;
	}

	if ( ! ::g_pTheTextureManager->Create2DTextureFromBMPFile("popcan.bmp", true) )
	{
		std::cout << "Couldn't load texture." << std::endl;
		bItsAllGoodMan = false;
	}

	// For the "fireball" thingy...
	if ( ! ::g_pTheTextureManager->Create2DTextureFromBMPFile("16228-flash.bmp", true) )	// <--- mask image
	{
		std::cout << "Couldn't load texture." << std::endl;
		bItsAllGoodMan = false;
	}
	if ( ! ::g_pTheTextureManager->Create2DTextureFromBMPFile("13982137-Fiery-Explosion-Seamless-Texture-Tile-Stock-Photo.bmp", true) )	// <--- "explosion" texture
	{
		std::cout << "Couldn't load texture." << std::endl;
		bItsAllGoodMan = false;
	}


	// Now we set up the sampler uniform locations. 
	// These are exactly the same as any other uniforms we've used, as they 
	//  represent a register in the GPU. Note that you CAN'T have sampler 
	//  arrays like you can with the lights. You CAN have sampler arrays, but they 
	//  are a very different thing: an "array" of 2D texture samplers is actually 
	//  implemented as a single 3D texture sampler, where you index the values by 
	//  the z (u) coordinate. We AREN'T looking at those as it's someone advanced. 
	// Bottom line: you have to get these one by one... 

	GLuint shaderID = ::g_pTheShaderManager->GetShaderIDFromName("basicShader");

	UniLoc_texSampler2D[0] = glGetUniformLocation(shaderID, "texSamp2D_00" );
	UniLoc_texSampler2D[1] = glGetUniformLocation(shaderID, "texSamp2D_01" );
	UniLoc_texSampler2D[2] = glGetUniformLocation(shaderID, "texSamp2D_02" );
	UniLoc_texSampler2D[3] = glGetUniformLocation(shaderID, "texSamp2D_03" );
	UniLoc_texSampler2D[4] = glGetUniformLocation(shaderID, "texSamp2D_04" );
	UniLoc_texSampler2D[5] = glGetUniformLocation(shaderID, "texSamp2D_05" );
	UniLoc_texSampler2D[6] = glGetUniformLocation(shaderID, "texSamp2D_06" );
	UniLoc_texSampler2D[7] = glGetUniformLocation(shaderID, "texSamp2D_07" );

	UniLoc_texMix[0] = glGetUniformLocation(shaderID, "textureMixRatios[0]");
	UniLoc_texMix[1] = glGetUniformLocation(shaderID, "textureMixRatios[1]");
	UniLoc_texMix[2] = glGetUniformLocation(shaderID, "textureMixRatios[2]");
	UniLoc_texMix[3] = glGetUniformLocation(shaderID, "textureMixRatios[3]");
	UniLoc_texMix[4] = glGetUniformLocation(shaderID, "textureMixRatios[4]");
	UniLoc_texMix[5] = glGetUniformLocation(shaderID, "textureMixRatios[5]");
	UniLoc_texMix[6] = glGetUniformLocation(shaderID, "textureMixRatios[6]");
	UniLoc_texMix[7] = glGetUniformLocation(shaderID, "textureMixRatios[7]");


	ExitOnGLError("ERROR in SetUpTextures().");


	return bItsAllGoodMan;
}

void SetupShader(void)
{
//	std::string error;
//	
	::g_pTheShaderManager = new CGLShaderManager();
	
	CShaderDescription vertShader;
//	vertShader.filename = "assets/shaders/SimpleShader.vertex.glsl";
	vertShader.filename = "assets/shaders/MultiLightsTextures.vertex.glsl";
	vertShader.name = "basicVert";
	vertShader.type = GLSHADERTYPES::VERTEX_SHADER;
	
	CShaderDescription fragShader;
//	fragShader.filename = "assets/shaders/SimpleShader.fragment.glsl";
//	fragShader.filename = "assets/shaders/MultiLightsTextures.fragment.glsl";
	fragShader.filename = "assets/shaders/MultiLightsTextures.fragment.spot.glsl";
	fragShader.name = "basicFrag";
	fragShader.type = GLSHADERTYPES::FRAGMENT_SHADER;
	
	CShaderProgramDescription basicShaderProg;
	basicShaderProg.name = "basicShader";
	basicShaderProg.vShader = vertShader;
	basicShaderProg.fShader = fragShader;
	
	if ( ! ::g_pTheShaderManager->CreateShaderProgramFromFile(basicShaderProg) )
	{	// Oh no, Mr. Bill!
		if ( ! basicShaderProg.bIsOK )
		{
			std::cout << "Error compiling the shader program" << std::endl;
			// Insert sexy error handling, logging code here... 
			for (std::vector<std::string>::iterator itError 
				 = basicShaderProg.vecErrors.begin();
				 itError != basicShaderProg.vecErrors.end(); 
				 itError++ )
			{
				std::cout << *itError << std::endl;
			}
			for (std::vector<std::string>::iterator itError 
				 = basicShaderProg.vShader.vecShaderErrors.begin();
				 itError != basicShaderProg.vShader.vecShaderErrors.end(); 
				 itError++ )
			{
				std::cout << *itError << std::endl;
			}	
			for (std::vector<std::string>::iterator itError 
				 = basicShaderProg.fShader.vecShaderErrors.begin();
				 itError != basicShaderProg.fShader.vecShaderErrors.end(); 
				 itError++ )
			{
				std::cout << *itError << std::endl;
			}		
		}
	}// if ( ! ::g_pTheShaderManager->...
	
	// Assume we are good to go...
	::g_pTheShaderManager->UseShaderProgram("basicShader");

	{
		CShaderProgramDescription compiledShader;
		compiledShader.name = "basicShader";
		::g_pTheShaderManager->GetShaderProgramInfo(compiledShader);
		for ( std::vector< CShaderUniformDescription>::iterator itUniform = compiledShader.vecUniformVariables.begin(); 
			  itUniform != compiledShader.vecUniformVariables.end(); itUniform++ )
		{
			std::cout << itUniform->index << ":" <<  itUniform->name << std::endl;
		}
	}

	//ShaderIds[0] = glCreateProgram();
	//ExitOnGLError("ERROR: Could not create the shader program");
	//{
	//	ShaderIds[1] = LoadShader("assets/shaders/SimpleShader.fragment.glsl", GL_FRAGMENT_SHADER);
	//	ShaderIds[2] = LoadShader("assets/shaders/SimpleShader.vertex.glsl", GL_VERTEX_SHADER);
	//	glAttachShader(ShaderIds[0], ShaderIds[1]);
	//	glAttachShader(ShaderIds[0], ShaderIds[2]);
	//}
	//glLinkProgram(ShaderIds[0]);
	//ExitOnGLError("ERROR: Could not link the shader program");


	SetUpUniformVariables();

	if ( ! SetUpTextures() )
	{
		std::cout << "Warning: One or more textures didn't load." << std::endl;
	}

	ExitOnGLError("ERROR in SetUpShaders()");

	return;
}

// Was "void DestroyCube()"
void ShutErDownPeople(void)
{
	//glDetachShader(ShaderIds[0], ShaderIds[1]);
	//glDetachShader(ShaderIds[0], ShaderIds[2]);
	//glDeleteShader(ShaderIds[1]);
	//glDeleteShader(ShaderIds[2]);
	//glDeleteProgram(ShaderIds[0]);
	//ExitOnGLError("ERROR: Could not destroy the shaders");

	::g_pTheShaderManager->ShutDown();

	::g_pTheMeshManager->ShutDown();

	::g_pTheTextureManager->ShutDown();

	// Go through the game object vector, deleting everything
	for ( std::vector< cGameObject* >::iterator itpGO = ::g_vec_pGOs.begin();
		itpGO != ::g_vec_pGOs.end(); itpGO++ )
	{
		// A little verbose, but maybe more clear?
		cGameObject* pCurGO = *itpGO;
		delete pCurGO;
	}
	::g_vec_pGOs.clear();


	//  glDeleteBuffers(2, &BufferIds[1]);
	//  glDeleteVertexArrays(1, &BufferIds[0]);

	delete ::g_pTheMeshManager;		
	delete ::g_pTheShaderManager;
	delete ::g_pTheTextureManager;

	ExitOnGLError("ERROR: Could not destroy the buffer objects");
}

//void DrawCube(void)
void DrawObject( cGameObject* pGO )
{
	if ( ! pGO->bIsVisible )
	{
		return;
	}

  float CubeAngle;
  clock_t Now = clock();

  if (LastTime == 0)
    LastTime = Now;

  CubeRotation += 45.0f * ((float)(Now - LastTime) / CLOCKS_PER_SEC);
  CubeAngle = DegreesToRadians(CubeRotation);
  LastTime = Now;

//  ModelMatrix = IDENTITY_MATRIX;
//  RotateAboutY(&ModelMatrix, CubeAngle);
//  RotateAboutX(&ModelMatrix, CubeAngle);
  //matView = glm::mat4(1.0f);

  matWorld = glm::mat4(1.0f);		// identity matrix

  glm::mat4 matWorldRotOnly(1.0f);		// For later (lighting)

  // STARTOF: From the Guts file... 
  // Rotation (post)
  matWorld = glm::rotate(matWorld, pGO->postRotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
  matWorld = glm::rotate(matWorld, pGO->postRotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
  matWorld = glm::rotate(matWorld, pGO->postRotation.z, glm::vec3(0.0f, 0.0f, 1.0f));

  //matWorldRotOnly = glm::rotate(matWorldRotOnly, pGO->postRotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
  //matWorldRotOnly = glm::rotate(matWorldRotOnly, pGO->postRotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
  //matWorldRotOnly = glm::rotate(matWorldRotOnly, pGO->postRotation.z, glm::vec3(0.0f, 0.0f, 1.0f));

  // Translation 
  matWorld = glm::translate(matWorld, 
	                        glm::vec3(pGO->position.x,
								      pGO->position.y,
								      pGO->position.z));

  // Rotation (pre)
  matWorld = glm::rotate(matWorld, pGO->preRotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
  matWorld = glm::rotate(matWorld, pGO->preRotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
  matWorld = glm::rotate(matWorld, pGO->preRotation.z, glm::vec3(0.0f, 0.0f, 1.0f));

  //matWorldRotOnly = glm::rotate(matWorldRotOnly, pGO->preRotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
  //matWorldRotOnly = glm::rotate(matWorldRotOnly, pGO->preRotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
  //matWorldRotOnly = glm::rotate(matWorldRotOnly, pGO->preRotation.z, glm::vec3(0.0f, 0.0f, 1.0f));

  // Or you can use an inverse transpose of the model matrix.
  // Strips off translation and scaling
  matWorldRotOnly = glm::inverse(glm::transpose(matWorld));

  // Scale 
  matWorld = glm::scale(matWorld, glm::vec3(pGO->scale, 
	                                        pGO->scale,
	                                        pGO->scale));
  // ENDOF: From the Guts file

  //glUseProgram(ShaderIds[0]);
  //ExitOnGLError("ERROR: Could not use the shader program");

  ::g_pTheShaderManager->UseShaderProgram("basicShader");


  glUniformMatrix4fv(ModelMatrixUniformLocation, 1, GL_FALSE, 
	                 glm::value_ptr(matWorld) );

  glUniformMatrix4fv(ViewMatrixUniformLocation, 1, GL_FALSE, 
	                 glm::value_ptr(matView) );

  glUniformMatrix4fv(ModelMatrixRotationOnlyUniformLocation, 1, GL_FALSE, 
	                 glm::value_ptr(matWorldRotOnly) );

	if ( pGO->bUseDebugColour )
	{
		glUniform4f( UniLoc_debugColour, pGO->debugColour.r, pGO->debugColour.g, 
					                     pGO->debugColour.b, pGO->debugColour.a );
		// Boolean 
		glUniform1f( UniLoc_bUseDebugColour, 1.0f /*TRUE*/ );	// Non zero
		//glUniform1i( UniLoc_bUseDebugColour, 1 /*TRUE*/ );	// Non zero)
	}
	else
	{	// Set the unform material stuff for the object
		glUniform1f( UniLoc_bUseDebugColour, 0.0f /*FALSE*/ );	// Non zero

		glUniform3f( UniLoc_MaterialAmbient_RGB, pGO->ambient.r, pGO->ambient.g, pGO->ambient.b );
		glUniform3f( UniLoc_MaterialDiffuse_RGB, pGO->diffuse.r, pGO->diffuse.g, pGO->diffuse.b );
		glUniform3f( UniLoc_MaterialSpecular, pGO->specular.r, pGO->specular.g, pGO->specular.b );
		glUniform1f( UniLoc_MaterialShininess, pGO->shininess );
	}
 
	if ( pGO->bUseVertexRGBAColoursAsMaterials )
	{
		glUniform1f( UniLoc_bUseVertexRGBAColours, 1.0f /*TRUE*/ );	// Non zero = true
	}
	else
	{
		glUniform1f( UniLoc_bUseVertexRGBAColours, 0.0f /*FALSE*/ );	// Non zero = true
	}

	// ********************************************************************************************
	// Textures.... 
	if ( pGO->bUseTexturesAsMaterials ) 
	{
		glUniform1f( UniLoc_bUseTextureMaterials, 1.0f /*TRUE*/ );		// Non zero = true
	}
	else
	{
		glUniform1f( UniLoc_bUseTextureMaterials, 0.0f /*FALSE*/ );		// Non zero = true
	}//if ( pGO->bUseTexturesAsMaterials ) 

	if ( pGO->bUseTexturesWithNoLighting )
	{
		glUniform1f( UniLoc_bUseTexturesOnly, 1.0f /*TRUE*/ );
	}
	else
	{
		glUniform1f( UniLoc_bUseTexturesOnly, 0.0f /*FALSE*/ );
	}//if ( pGO->bUseTexturesWithNoLighting )

	// ********************************************************************************************
	// If we are using textures, set the "texture mix" values for this object
	if ( pGO->bUseTexturesAsMaterials || pGO->bUseTexturesWithNoLighting )
	{
		for ( unsigned int index = 0; index != NUMBEROF2DSAMPLERS; index++ )
		{	// Double-check that there actually IS a mix value in the object...
			if ( index < pGO->vecTextureMixRatios.size()  )
			{	// Set it from the object
				glUniform1f( ::UniLoc_texMix[index], pGO->vecTextureMixRatios[index] );
			}
			else
			{	// This object doesn't have a mix value at that location
				glUniform1f( ::UniLoc_texMix[index], 0.0f );
			}
		}
	}//if ( pGO->bUseTexturesAsMaterials || pGO->bUseTexturesWithNoLighting )


	ExitOnGLError("ERROR: Could not set the shader uniforms");

	std::string modelToDraw = pGO->modelName;

	cVBOInfo curVBO;
	if (!::g_pTheMeshManager->LookUpVBOInfoFromModelName(modelToDraw, curVBO))
	{ // Didn't find it.
		return;
	}

//  glBindVertexArray(BufferIds[0]);
  glBindVertexArray(curVBO.VBO_ID);


  ExitOnGLError("ERROR: Could not bind the VAO for drawing purposes");

  // Make everything lines ("wireframe")
  if ( pGO->bIsWireframe )
  {
	  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	  glDisable(GL_CULL_FACE);	// Enable "backface culling
  }
  else
  {  
	  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	  glEnable(GL_CULL_FACE);	// Enable "backface culling
  }


//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//glDisable(GL_CULL_FACE);	// Enable "backface culling
	  
  // Transparency
	glEnable( GL_BLEND );		// Enables "blending"
	// Source == already on framebuffer
	// Dest == what you're about to draw
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	if ( pGO->bIsEntirelyTransparent )
	{	// Set alpha for ENTIRE object
		glUniform1f( UniLoc_bAlphaForEntireObject, 1.0f /*TRUE*/ );
		glUniform1f( UniLoc_myAlphaAllObject, pGO->alphaValue);	// 1.0 is NOT transparent	
	}
	else
	{	// Don't GLOBALLY set transparency
		glUniform1f( UniLoc_bAlphaForEntireObject, 0.0f /*FALSE*/ );
		glUniform1f( UniLoc_myAlphaAllObject, 1.0f );	// 1.0 is NOT transparent	
	}

	if ( pGO->bUseDiscardMask )
	{	// Set up discard mask in shader
		glUniform1f( UniLoc_bUseDiscardMask, 1.0f /*TRUE*/ );
	}
	else
	{	
		glUniform1f( UniLoc_bUseDiscardMask, 0.0f /*TRUE*/ );
	}



  unsigned int numberOfIndicesToDraw = curVBO.numberOfTriangles * 3;

  glDrawElements(GL_TRIANGLES, numberOfIndicesToDraw,	// 36,
	             GL_UNSIGNED_INT, 
	             (GLvoid*)0);
  ExitOnGLError("ERROR: Could not draw the cube");

  glBindVertexArray(0);
//  glUseProgram(0);
  ::g_pTheShaderManager->UseShaderProgram(0);

  return;
}


void SetUpInitialLightValues(void)
{
	// Assume the lights are loaded into the g_vecLights;
	::g_vecLights[0].position = glm::vec4( 0.0f, 3.0f, 0.0f, 1.0f );
	::g_vecLights[0].diffuse =glm::vec4( 1.0f, 1.0f, 1.0f, 1.0f );
	::g_vecLights[0].ambient =glm::vec4( 0.2f, 0.2f, 0.2f, 1.0f );
	::g_vecLights[0].specular =glm::vec4( 1.0f, 1.0f, 1.0f, 1.0f );
	::g_vecLights[0].attenConst = 0.0f;
	::g_vecLights[0].attenLinear = 0.2f;
	::g_vecLights[0].attenQuad = 0.1f;

	// Assume the lights are loaded into the g_vecLights;
	::g_vecLights[1].position = glm::vec4( 0.0f, 3.0f, 0.0f, 1.0f );
	::g_vecLights[1].diffuse =glm::vec4( 1.0f, 1.0f, 1.0f, 1.0f );
	::g_vecLights[1].ambient =glm::vec4( 0.2f, 0.2f, 0.2f, 1.0f );
	::g_vecLights[1].specular =glm::vec4( 1.0f, 1.0f, 1.0f, 1.0f );
	::g_vecLights[1].attenConst = 0.0f;
	::g_vecLights[1].attenLinear = 0.2f;
	::g_vecLights[1].attenQuad = 0.1f;
	
//	::g_vecLights[1].lightType = 1;	// Spot
	::g_vecLights[1].lightType = 0;	// point
	//::g_vecLights[1].direction = glm::vec4(0.0f, -1.0f, 0.0f, 1.0f);
	//::g_vecLights[1].anglePenumbraStart = 15.0f;
	//::g_vecLights[1].anglePenumbraEnd = 60.0f;
}


void CreateDefaultObjects(void)
{
	// Now with more ply...


	g_pDebugBall = new cGameObject();
	g_pDebugBall->modelName = "assets/models/Isoshphere.ply";
	g_pDebugBall->debugColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	g_pDebugBall->bUseDebugColour = true;
	g_pDebugBall->bIsWireframe = true;
	g_pDebugBall->scale = 1.0f;
	g_pDebugBall->bIsADebugObject = true;

	//cGameObject* pInvertBall = new cGameObject();
	//pInvertBall = new cGameObject();
	//pInvertBall->modelName = "assets/models/Isoshphere_normalsFacingIn.ply";
	////pInvertBall->debugColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	////pInvertBall->bUseDebugColour = true;
	//pInvertBall->diffuse = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	//pInvertBall->ambient = glm::vec4(0.2f, 0.2f, 0.2f, 1.0f);
	//pInvertBall->scale = 3.0f;
	//pInvertBall->position.z = 2.0f;
	//pInvertBall->position.y = 2.0f;


	// Tie is object zero 
//	cGameObject* pBlueWhale = new cGameObject();
//	pBlueWhale->modelName = "assets/models/BlueWhale.ply";
//	pBlueWhale->debugColour = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
//	pBlueWhale->diffuse = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
//	pBlueWhale->ambient = glm::vec4(0.2f, 0.2f, 0.2f, 1.0f);
//	pBlueWhale->specular = glm::vec3(1.0f, 1.0f, 1.0f);
//	pBlueWhale->scale = 35.0f;
//	pBlueWhale->preRotation.x = glm::radians( +90.0f );
//	//pBlueWhale->preRotation.z = glm::radians( -135.0f );
////	pBlueWhale->position.z = 1.0f;
//	pBlueWhale->position.z = -2.0f;
//	pBlueWhale->position.x = 3.5f;
//	pBlueWhale->shininess = 1.0f;
//
//	pBlueWhale->bUseTexturesAsMaterials = true;
//	//pBlueWhale->bUseTexturesWithNoLighting = true;
//	// Set up 8 textures in the model
//	pBlueWhale->ClearTextureMixValues(NUMBEROF2DSAMPLERS, 0.0f);
//	// Use texture #0, don't mix with any others
//	pBlueWhale->vecTextureMixRatios[0] = 0.0f;		// Brick texture
//	pBlueWhale->vecTextureMixRatios[1] = 0.0f;		// Blue tiles
//	pBlueWhale->vecTextureMixRatios[2] = 0.0f;		// Gold leaf
//	pBlueWhale->vecTextureMixRatios[3] = 1.0f;		// Blue Whale
//	pBlueWhale->vecTextureMixRatios[4] = 0.0f;		// UV coloured grid thing
//
//	pBlueWhale->bIsEntirelyTransparent = true;
//	pBlueWhale->alphaValue = 0.75f;
//
//	// Tie is object zero 
//	cGameObject* pTieFighter = new cGameObject();
//	pTieFighter->modelName = "assets/models/tie_Unit_BBox.ply";
//	pTieFighter->debugColour = glm::vec4(1.0f, 0.0f, 1.0f, 1.0f);
//	pTieFighter->diffuse = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
//	pTieFighter->ambient = glm::vec4(0.2f, 0.0f, 0.2f, 1.0f);
//	pTieFighter->specular = glm::vec3(1.0f, 1.0f, 1.0f);
//	pTieFighter->scale = 2.0f;
//	pTieFighter->preRotation.x = glm::radians( 90.0f );
//	pTieFighter->preRotation.z = glm::radians( 90.0f );
//	pTieFighter->position = glm::vec3(2.0f, 0.0f, 2.0f);
//	pTieFighter->shininess = 1.0f;
//
//
//	//cGameObject* pBunny = new cGameObject();
//	//pBunny->modelName = "assets/models/bun_zipper_hiRez.ply";
//	//pBunny->debugColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
//	//pBunny->diffuse = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
//	//pBunny->ambient = glm::vec4(0.2f, 0.2f, 0.2f, 1.0f);
//	//pBunny->specular = glm::vec3(1.0f, 1.0f, 1.0f);
//	//pBunny->shininess = 1000.0f;			// 1.0 to 100,000 
//	//pBunny->scale = 50.0f;
//	////pBunny->position.x = -3.0f;
//	//pBunny->position = glm::vec3(-1.96708f, -3.57167f, -2.23921f);
//
//	//pBunny->bUseDiscardMask = true;		// Assume mask is in texture unit 7
//
//
//	cGameObject* pSplat = new cGameObject();
//	pSplat->modelName = "assets/models/1x1_6_Cluster_2_Sided_xyz_nxyz_uv.ply";
//	pSplat->debugColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
//	pSplat->diffuse = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
//	pSplat->ambient = glm::vec4(0.2f, 0.2f, 0.2f, 1.0f);
//	pSplat->specular = glm::vec3(1.0f, 1.0f, 1.0f);
//	pSplat->shininess = 1000.0f;			// 1.0 to 100,000 
//	pSplat->scale = 3.0f;
//	//pBunny->position.x = -3.0f;
//	pSplat->position = glm::vec3(0.0f, 3.0f, 2.0f);
//
//	pSplat->bUseDiscardMask = true;		// Assume mask is in texture unit 7
//	// To use the explosion texture, we need to set the "mix" ratios, too
//	pSplat->ClearTextureMixValues(NUMBEROF2DSAMPLERS, 0.0f);
//	// "Explosion" is texture unit #6 (see SetTextureBindings() function for this)
//	pSplat->vecTextureMixRatios[6] = 1.0f;	// All explosion, all the time...
//	pSplat->bUseTexturesAsMaterials = true;		// Tell the shader we want textures
//	pSplat->bUseTexturesWithNoLighting = false;	// And that we DON'T want lighting (as it's an explosion) 
//	                                            // - you could change this, if you had a light right in the 
//	                                            //   centre of the explosion, for instance
//
//
//
//	cGameObject* pMig = new cGameObject();
//	pMig->modelName = "assets/models/mig29_xyz.ply";
//	pMig->debugColour = glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
//	pMig->diffuse = glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
//	pMig->ambient = glm::vec4(0.0f, 0.2f, 0.0f, 1.0f);
//	pMig->specular = glm::vec3(1.0f, 1.0f, 1.0f);
//	pMig->shininess = 1.0f;
//	pMig->position.x = 3.0f;
//
//	cGameObject* pGround = new cGameObject();
//	pGround->modelName = "assets/models/Seafloor2.ply";
//	pGround->debugColour = glm::vec4(0.5f, 0.5f, 0.5f, 1.0f);
//	pGround->diffuse = glm::vec4(0.5f, 0.5f, 0.5f, 1.0f);
//	pGround->ambient = glm::vec4(0.1f, 0.1f, 0.1f, 1.0f);
//	pGround->specular = glm::vec3(1.0f, 1.0f, 1.0f);
//	pGround->shininess = 1.0f;
//	pGround->scale = 0.1f;
//	pGround->position.y = -3.0f;
//
//
//	::g_vec_pGOs.push_back( pTieFighter );
////	::g_vec_pGOs.push_back( pBunny );
//	::g_vec_pGOs.push_back( pMig );
//	::g_vec_pGOs.push_back( pGround );
//	::g_vec_pGOs.push_back( pBlueWhale );
//	//::g_vec_pGOs.push_back( pInvertBall );
//	::g_vec_pGOs.push_back( pSplat );




	return;
}
